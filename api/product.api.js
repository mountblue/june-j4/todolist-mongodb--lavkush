var mongoose = require('mongoose');
var Product = require('../schemas/product.schema');

var ProductAPI = {
    create: function (request, response) {
        var newProduct = new Product({
            value: request.body.value,
            id: request.body.id,
            status: request.body.status
        });

        newProduct.save(function (error) {
            if (error) {
                throw error;
            } else {
                response.status(200).json(newProduct);
            }
        })
    }
}
modual.exports=ProductAPI;n