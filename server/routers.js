
var bodyParser = require('body-parser');
var express = require('express');
var router = express.Router();
var Todos = require('../model/todos');
router.get('/', function (req, res) {
  res.render('index')
});

router.get('/todos', function (req, res) {
  Todos.find({}).then(function (data) {
    res.json(data)
  })
})

router.post('/todos', function (req, res) {
  Todos.create(req.body).then(function (data) {
    res.json(data);
  })
})

router.delete('/todos/:id', function (req, res) {
  let objectId = req.params.id;
  console.log(objectId)
  Todos.findByIdAndRemove({ "_id": objectId }).then(function (data) {
    res.json(data);
  })
})

router.put('/todos/:id', function (req, res) {
  let objectId = req.params.id;
  let objectStatus = req.body.ckh;
  Todos.findByIdAndUpdate(objectId, {
    status: objectStatus
  }).then(function (data) {
    console.log(data);
    res.send(data);
  })
})

module.exports = router;