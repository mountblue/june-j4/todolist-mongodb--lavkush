var mongoose = require('mongoose');
var TodosSchema = new mongoose.Schema({
    value: String,
    _id: String,
    status: Boolean
})

Todos = mongoose.model('todos', TodosSchema);

module.exports = Todos;