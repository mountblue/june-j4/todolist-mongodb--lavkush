import React from 'react';
import CreateElement from './TodoItem';
import id from 'uuid';
class App extends React.Component {
    constructor() {
        super();
        this.state = {
            itemList: [],
        };
        this.addElement = this.addElement.bind(this);
        this.removeElement = this.removeElement.bind(this);
        this.checkList = this.checkList.bind(this);
    }


    componentWillMount() {
        fetch('/api/todos', {
            method: 'GET'
        })
            .then(response => response.json())
            .catch(error => console.error('Error:', error))
            .then(todoData => this.setState({ itemList: todoData })
            );
    }


    removeElement(itemId) {
        let items = this.state.itemList
        let filterArray = items.filter(item => {
            return item._id !== (itemId)
        });
        this.setState({ itemList: filterArray });
        fetch('/api/todos/' + itemId, {
            method: 'Delete',
        }).then(res => res.json())
            .catch(error => console.error('Error:', error));
    }
    checkList(itemId) {
        let items = this.state.itemList;
        let dbItem;
        let filterData = items.filter(item => {
            if (item._id == itemId) {
                if (item.status) {
                    dbItem = false;
                    item.status = false;
                } else {
                    dbItem = true;
                    item.status = true;
                }
            }
            return item
        });
        this.setState({ itemList: filterData });
        fetch('/api/todos/' + itemId, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ ckh: dbItem }),
        }).then(res => res.json()).then(this.componentWillMount())
            .catch(error => console.error('Error:', error));

    }

    addElement() {
        let inputText = this.refs.tp.value;
        if (inputText.length !== 0) {
            let objectOfState = {}
            objectOfState["value"] = inputText;
            objectOfState["_id"] = id.v4();
            objectOfState["status"] = false;
            let items = this.state.itemList;
            items.concat([objectOfState]);
            fetch('/api/todos', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(objectOfState),
            }).then(res => res.json()).then(this.componentWillMount())
                .catch(error => console.error('Error:', error));
        }
    };
    render() {
        return (
            <div>
                <div className="inputContainer">
                    <input
                        ref="tp"
                        type="text"
                        name="nm"
                    />
                    <button
                        onClick={this.addElement}
                    >
                        Add List
                    </button>
                </div>

                {this.state.itemList.map(tag => <CreateElement key={tag._id}
                    value={tag.value} id={tag._id} clickStatus={tag.status}
                    onDelete={this.removeElement} checkStatus={this.checkList} />)}

            </div>
        );
    }
}
export default App;