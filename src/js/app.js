
import '../css/style.css';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './TodoList';

ReactDOM.render(<App />, document.getElementById('app'));