
import React from 'react';
class create extends React.Component {
    constructor(props) {
        super(props)
        this.checkE = this.checkE.bind(this);
        this.checkBoxF = this.checkBoxF.bind(this);
    }
    checkBoxF(event) {
        this.props.checkStatus(event.target.id);
    }
    checkE(event) {
        this.props.onDelete(event.target.id)
    }
    render() {
        return (
            <div className="task-style">
                <input className="commStyle checkStyle" type="checkbox" onClick={this.checkBoxF} id={this.props.id} />
                <div className="commStyle textStyle" className={this.props.clickStatus ? "checked" : "unchecked"} id={this.props.id}>{this.props.value}</div>
                <button className="commStyle buttonStyle" onClick={this.checkE} id={this.props.id}>delete</button>
            </div>
        );
    }
}
export default create;