var express = require('express');
var router = require('./server/routers')
var path = require('path');
var bodyParser = require('body-parser');
var app = express();
var port = 9000
var mongoose = require('mongoose');
app.use(express.static('dist'))
app.use(express.static('src'))
app.use(bodyParser.json());
app.use('/api', require('./server/routers'))
mongoose.connect('mongodb://127.0.0.1:27017/todoData', { useNewUrlParser: true });
app.use('/', router);

app.get('*', function (req, res) {
    res.sendFile(__dirname + '/dist/index.html');
});

app.listen(port, function () {
    console.log('running at localhost: ' + port);
});

console.log("inside server.js file");

module.exports = app;